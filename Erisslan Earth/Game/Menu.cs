﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            Form SettingsForm = new Settings();
            SettingsForm.ShowDialog();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            Form FormToPlay = new Game();
            FormToPlay.ShowDialog();
        }
    }
}
